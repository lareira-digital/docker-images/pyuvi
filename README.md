# Pyuvi

[![pipeline status](https://gitlab.com/lareira-digital/docker-images/pyuvi/badges/0.1.0/pipeline.svg)](https://gitlab.com/lareira-digital/docker-images/pyuvi/-/commits/0.1.0)
[![Latest Release](https://gitlab.com/lareira-digital/docker-images/pyuvi/-/badges/release.svg)](https://gitlab.com/lareira-digital/docker-images/pyuvi/-/releases) 

This is a base image for Lareira Projects, or for your project, if you use Python 3
and this fits what you need :)

# Docker Hub
https://hub.docker.com/repository/docker/lareiradigital/pyuvi/general

# Build ARGS

ALPINE_VERSION (Python version is determined based on that)
